﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp
{
    class DigitalBook:Book
    {
        //creating field variables
        private int _maxBorrowDays;
        private float _latePenatlyPerDay;

        //contructor for object of type DigitalBook
        public DigitalBook(string bookName, string bookISBN):base(bookName,bookISBN){
            _maxBorrowDays = 0;
            _latePenaltyPerDay = 0.0f;
        }

        //method to determine the loan license
        private void DetermineLoanLicense() {
            Random num = new Random();
            _maxBorrowDays = num.Next(14,56);
            _latePenatlyPerDay = (float)0.1 + (float)num.NextDouble() * (float)0.4;
        }

    }
}
