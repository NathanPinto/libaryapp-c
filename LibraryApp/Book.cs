﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp
{
    class Book
    {
        //creating field variables
        private string _bookName;
        private string _bookISBN;
        private List<String> _bookAuthorList;
        private List<LibraryAsset> _libAssetList;

        //creating properties all read only

        //property that gets the name of the book
        public string Name{
            get{ return _bookName; }
        }

        //property that gets the ISBN of the book
        public string ISBN{
            get{ return _bookISBN; }
        }

        //property that gets the Authors of the book
        public List<String> Authors{
            get{ return _bookAuthorList; }
        }

        //property that gets the Assets of the book
        public IEnumerable<LibraryAsset> Assets{
            get{ return _libAssetList; }
        }

        //contructor method for creating an Object of type Book
        public Book(string bookName, string bookISBN){
            _bookName = bookName;
            _bookISBN = bookISBN;
            _bookAuthorList = new List<String>();
            _libAssetList = new List<LibraryAsset>();
        }

        //finds library asset
        public LibraryAsset FindLibraryAsset(int libID){
            foreach (LibraryAsset assetFound in _libAssetList){
                if(assetFound.LibID == libID){
                    return assetFound;
                }
                else{
                    print("Asset not Found");
                }
            }
        }
        



    }
}
