﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LibraryApp
{
    class LibraryAsset
    {
        //creating field variables
        private Book _book;
        private int _libID;
        private AssetStatus _status;
        private LoanPeriod _loanPeriod;

        //creating properties 

        //property that gets the LibID
        public int LibID{
            get{ return _libID; }
        }

        //property that gets and sets the AssetStatus
        public AssetStatus Status{
            get{ return _status; }
            set{ _status = value; }
        }

        //property that gets and sets the LoanPeriod
        public LoanPeriod Loan{
            get{ return _loanPeriod; }
            set{ _loanPeriod = value; }
        }

        //property that gets availability
        public bool isAvailable{
            get{ return _status; }
        }

        //contructor for an object of type LibraryAsset
        public LibraryAsset(int libID, Book book){
            _libID = libID;
            _book = book;
        }

        //creating enum for AssetStatus
        public enum AssetStatus{
            NOTAVAILABLE = 1,
            AVAILABLE = 2,
            LOANED = 3,
            RESERVED = 4
        }

        //creating struct for LoanPeriod
        public struct LoanPeriod{
            //creating field variables
            private DateTime _borrowedOn;
            private DateTime _returnedOn;
            private DateTime _dueDate;

            //creating properties

            //property that gets and sets borrowedOn date
            public DateTime BorrowedOn{
                get{ return _borrowedOn; }
                set{ _borrowedOn = value; }
            }

            //property that gets and sets the returnedOn date
            public DateTime ReturnedOn{
                get{ return _returnedOn; }
                set{ _returnedOn = value; }
            }

            //creating property that gets and sets the DueDate date
            public DateTime DueDate{
                get{ return _dueDate; }
                set{ _dueDate = value; }
            }

            //property that gets the LoanDuration
            public LoanPeriod LoanDuration{
                get{ return _returnedOn-_borrowedOn; }
            }

            //property that gets the LatePeriod
            public TimeSpan LatePeriod{
                get{ return _returnedOn-_dueDate; }
            }

            //constructor for struct
            public LoanPeriod(DateTime borrowedOn, DateTime returnedOn, DateTime dueDate){
                _borrowedOn = borrowedOn;
                _retunedOn = returnedOn;
                _dueDate = dueDate;
            }

        }



    }

}
